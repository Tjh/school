//Lec May 10
//Basic Argumentation and Constructing and Evaluating moral argument. 


Moral Arguments
-----------------

-> Good Arguments; how to tell:
|-> Solid Logic
\-> True Premise.

-> 2 types of arguments
|-> Deductive: strongest type
\-> Inductive: probably true. 

-> Deductive arguments (true if):
|-> If conc logically follows from presmise, argument is "solid". (Validity)
\-> If premise is true then argument is considered "sound".

-> Common/Valid forms:
	1. X's are A's
	   i is an X
	   Thus, i is an A.
	2. If P => Q       }--Modus Ponens (Affirming the antecedent)
	   P therefore Q.  }/
	3. If P => Q       }--Modus Tollens (Denying the Consequent)
	   !Q therefore !P }/

If P then Q <- Conditional.
|->If P = Antecedant; Then Q = consequedant.
\-> Names of arguments derive from the structure of the premise (e.g. Denying the consequent means that the argument is based on denying the consequent; i.e. !Q is the main premise). 

Invalid Forms (Conclusion does not follow).
1. Affirming the Consequent:
   	If P => Q
    	Q
	thus, P. 
   \-> In the above example, there is nothing preventing Q from arising from some other condition (e.g. B=>Q), which means that Q can be true even without P. However if the conditional was "IFF P=>Q" then the above would be true, since then Q must arise out of P. 

2. Denying the Precedant
	If P=> Q
	!P
	thus !Q. 
   \-> Untrue for the same reason as above, Q can arise from !P. 

Inductive
----------

-> Move arguments are inductive (esp. in moral/ethics).
|-> If the premise is true conc. is probably true, but there is a chance it is untrue.
\-> Usually used for science as well, which is why scientific conclusions are always about "% chance of being true" and not defenite.

-> If the argument lends good support then the argument *is strong*
\-> If the argument is strong as above, AND the premise is true then the argument is `cogent'.

Moral Argumentation
--------------------
-> Moral Statements are usually an 'ought' as opposed to normal statements which are 'is'. 
-> Fallacies:- certain fallacies are common in moral arguments, more than other logical fallacies:
	1. Ad Hominem: Against the man (attacking person instead of argument).
	2. Improper Authority (Common in religion based ethics). 
	3. Circular Argument: Conclusion is part of premise, doesn't have to explicitly stated in the premise(s) just part of it, or needed in the premise). 
	4. Appeal to Ignorance: Using lack of evidence as proof.
	5. False Dilemma: Reducing a complex choice, to its polar opposites so aas to drive people to one side, even though there are middle grounds etc. 
	6. Slippery Slope: Taking the precedant and 'running with it'.
	7. Straw Man: Oversimplification of opponents point to ease the argument	8. Genetic Fallacy: Arguing about something by pointing out it's genes (i.e. ancestory). Basically; "X is bad because it came from A which is a bad place". 

-> Moral Arguments: Must have at least one moral premise and one non-moral premise. 
|-> Moral judgements cannot be made without a moral premise cuz we cannot infer and ought (moral statement) from an is (non moral statements).  
|-> Non moral statements need to be thre so as to connect the argument to a general logical structure, and the "real world" as such. 
\-> Either of these can be hidden, but they have to be present in some way or form.

-> Moral premises can be called into question by generating counter-examples, i.e. situations where such a premise would be innaplicable or obviously mistaken to apply. 

-> Evaluating moral arguments: 
|-> Evaluate the logic.
|-> Check the non-moral premise; the data they cite etc. 
|-> Ensure the interpertation of the 'terms used' are understood as they were meant to be. 
\-> Truth of moral statement - comes from 3 sources:
	1. Moral principles
	2. Moral Theories (valid moral theories can provide already worked out principles beyond those that are readily accepted by society).
	3. Relaible and considered moral judgements (statements that have been generally tested and accepted by a large/reasonable number of people. e.g: ``killing is wrong"). 
    \-> Evaluate these sources and the truth of the arguments used in them. Inconsistencies in the claims can also present a problem.
	 
