//Lecture May 05: Theogony, Creation and Zeus\/The Pantheon

Polytheism
----------

* In the west often due to the abrahamic religion, people are used to a singular diety. (Something I have seen time and time again in philosophical discussions about God). 
* Just like hinduism, the greeks had no issue in seeing the Gods change, and certain people worshipping many dieties or only some of the gods. 
** Polytheism also makes it easy for people to move seamlessly between religions ("Japanese are born shinto, marry christian and die buddhist"). 
** It also makes it easy for accept various religions etc. 
** There is a Pan cultural idea of certain gods, but each of these dieties are worshipped in different ways in different areas.
** Pan-Hellenic Cult: the pan cultural idea of the god, there are certain ideas that are "pan" the whole greco-roman societies.
*** There are also local cults, local cults often have pan hellenic ideas (for example there are local cults related to athena, which has ideas of the pan hellenic athena). 
*** There are thus changing ideas of the god, for example athena is both related to war, but also related to maiden craftsmanship (c.f. the quotation in page 59). 
* `Hymn to aphrodite' quote - talking about athena.  
* The flexibility in viewing gods, perhaps allows people to see more of a connection between gods and humans and also deify humans.

Theogony
---------
* Theogony = how the gods/universe came to be. 
* Enuma-Elish: near eastern theogonic poem that seem to have influenced greco-roman theogony, esp. hesiod's theogony.
* We are looking at Hesiod's Theogony, the poem he wrote about creation/theogony: 
** Most descriptions of the world etc, were in fact poetry. 
** Written (or at least composed in some way) around 700BC. We know this due to references made to historical events.
** No one knows wether or not Hesiod's poem or Homer's Odessey/Illiad is older. But it was understood in greek culture that Hesiod's was older, perhaps due to the fact that it deals with an event that is seen as earlier, i.e. the creation.
** Lines 1 - 115: Hymn and Invocation to Muses. (Muses = children of Zeus and Mnemosyne who were seen as inspiration to the arts and were invoced in artistic works to help inspire the artist). Calliope (muse of epic poetry) is the one who is said to inspire the poem. 
*** Muse: It is important to 
** Even though his poem deals with a certain geographical area in detail, it is very much pan-hellenic and sets the bar for a pan-cultural understanding of the gods. 
** Line 116 - 122: Tartarus = the lowest realm of the worlds. Even below hades.
*** Kaos = The absense of anything, nothingness into which things come in., and not "Chaos" as it is understood today.
*** The eros that is seen as a part of the primordial beings is not the same as the one that is known as cupid, this one is more of a primordial 'love'.
** Lines 123 - 153: The second generation comes out of gaia, and gives birth via parthenogenesis. 
*** Note that the first being, and most important one here, *is* in fact a woman, but across Hesiod's work we can see an overarching theme of a patriarchy taking over eventually. The greek world was defenitely patriarchal esp. at the time of the writing of this work.       
** Lines 154 - 210: (First succesion myth) Ouranuos was afraid of being overthrown by kronos, and thus stayed within gaia (physically) whilst making love, and thus did not let Kronos out. Kronos eventually, and with Gaia's help cut of the genitals of Ouranos, thus getting out and overthrowing Ouranos. 
*** Out of the genital of Ouranos comes Aphrodite. 
*** This is a very interesting tale to look at allegorically, speaking about the splitting of Sky and Earth.
** Rhea and Kronos are the parents of Zues. Rhea, is the counterpart of Gaia, they give birth to many olympian dieties, they are the older generation, there are some that are sons of Zeus and the sons/daughters of Rhea/Kronos. 
** Lines 453 - 506 (The second succesion): Kronos, being afraid again of being overthrown by the youngest son; swallows all the gods. Swallowing Zeus last, Rhea helps zeus put a stone in place of himself, the stone then causes all the swallowed gods to be regurgitated.
** Lines 617 - 731: Battle of the Titans, zeus wins.
** Zeus later becomes afraid of his wife (Metis=wisdom). He swallows metis, after which he gives birth to athena by himself (coming out of his head). The swallowing of metis might represent zeus' joining with feminine qualities, giving him great wisdom (in this case it is the female aspect that gives him wisdom, and birth athena, a god known for wisdom). 
** Lines 820 - 855: The last challenge of Zeus; The giants and Typhoeus. (Typhoeus - born of Giaia and Tartarus, the father of all `monsters' ). 
** The later parts of the poem; describe advice for humans for farming etc (Wisdom literature). And later describes the Human creation.
*** He has 4 or 5 ages from the creation of man: Age of Gold (everyone is happy), age of Silver (less praying and respect to gods, not everyone is happy), age of bronze (people are not very happy) and Age of iron (you don't wanna live here, the age at the time of Hesiod). 
*** There is something between age of Bronze and Iron which does not go into the theme of a steady decline since mankinds creation: the age of heroes; is a time of heroes etc. [[ Important: c.f. Parellels to Hindu understanding of the 4 ages etc ]].  

The Gods: Prometheus
---------------------
* A very important 'God'. 
* He very famously acts against zeus. He does help zeus in the war with the titans however, but goes against zeus in various things, esp. to benifit mankind. 
** He tricks zeus first, to take the bad part of the animal during sacrifice (the bones, instead of the meat). He wrapps the bones around the fat, thus making it seem as if it is tasty and good, thus leaving the meat for the humans.
*** This is an explanation of the habit of eating the good part of the animal after a sacrifice, even though the gods were supposedly taking part of the sacrifice (explaining why it is that the meat is left for the human, as we eat it afterwards). 
* He also steals fire, for which he was punished (the famous liver/bird thing). Those who went against zeus, even gods, were forced to suffer such horrific punishments.
* In a non-hesiod tale of creation of man, prometheus is the one incharge of putting the clay together. (c.f. pg. 65, theogony and creation pdf)
* Prometheus' grandchild pyrrha and Deucalion is an important figure in a myth similar to Noah's ark (c.f. pg 76 - 79). 
pg 80: zeus punishing mankind
pg 81: the two; pyrrha and duecalion going away from the temple, as they escape and are the only ones to survive the flood, just as noah was in the myth of Noah's ark.

The Gods: Pandora and the creation of Woman
---------------------------------------------
* The first woman was said to have various gifts from every olympian god. 
* She opens a 'Jar' given to her letting out various evils (which goes to say a lot about woman and evils), and of course the hope that is left in the box (creation of hope, related to woman). 
* But hope is in the box (everything else in there is evil), which can mean various things, like the role of woman in bringing hope into the world, and zeus giving hope to Pandora thru the box. 

The Gods: Zeus
---------------
* He is known as the father of the gods. Not only is he *actually* the father of a lot of the gods, this also calls to his power over the gods.
* He is often seen with a beart  due to his age.
* Related to 'Dyaushputr': sansk. for sky-father, son of agni and prithvi. 
* Zeus seen in earlier works holding an Aegis (a sort of staff).
* Was worshipped a number of different ways, example 'zeus the liberator' (pg. 5, Zeus pdf).
** Also here is a inscription talking about the worship of zeus, thanking him as responsible for the victory.
* He has lots of affairs:
** pg.7 of Zeus pdf: Hera tries to seduce zeus, because she wanted a war to go a specific way, she takes help from hera, the speech here is when hera says that she must instead of sleeping with him, go to visit her parents. She is succuesful ofc.
** There are lots of stories like this of zeus getting tricked, but it is interesting to note that a. they are of both sexes (the affairs) and b. he never actaually loses control of the cosmos, all the tricks are small embaressing things and/or things that affect mankind. 
* Zeus and Danae - Danae's father shuts her into a room because he was told that her son would overthrow him. ofc, zeus can get into the room even if no 'man' can.
* Zeus punishes aphrodite for making him fall in love with so many mortal women. 
* zeus and europa: zues turns into a bull and takes europa from a field picking flowers, to crete, and has sex with her there, giving birth to king minos and the creteian descent and the minotaur (cuz zeus was a bull at the time of the affair).
* zeus and Io: the anger of hera shows up in this affair, she turns Io into a cow and puts him under the guard of a great snake, in response to the affair. 
* Interestingly: Gods always have affairs etc, but never really suffer, unlike humans.
* Sarpedon: this mythology shows how zeus must in fact (at times at least) give in to fate, even if he is the ruler of the gods. 
** we see here the 'zeus of justice' as he tries to decide if or not to save his son. 
* Olympics: a religious festival for zeus, who oversaw this festival, he rules over the victories and the festival. One of the greatest cults of Zeus *is* in fact in Olympia. Zeus here, is also considered the 'god of victory' (c.f. previously the inscription calling zeus and thanking him for the victory`). 