### PHIL 402
### Week 1, Lec 1
### Epistemic Ignorance.

Epistemology
------------

-> Aims of epistemology: 
	1. To achieve a general understanding of knowledge, belief, justification, truth and *testimony* etc. either through conceptual analysis or by reflection on how they are used in practice. 
   	2. Understanding what it is to know something vs having having a true belief. 
	3. Determine the limits of knowledge (ignorance - how it is constructed etc).
	4. Refute scepticism. (Trying to prove that we do in fact have some knowledge)
	\-> We are going to assume in this class that we *do* in fact have knowledge.  
-> In mainstream anglo-american theory of knowledge there has been a lot of attention to the difference between `knowing something' and having a `true belief' that is mainly focused on propositional knowledge of the form "S knows P". The 'S' assumes that subjects are interchangable, i.e. that any subject/person claiming to "know P" is the same as any other, in terms of epistemology. 
\-> However in this class we observe that not all subjects are interchangable, in that the subjects are all different kinds of people whose knowledge is different. I.e. A person in a marginalised group claiming to know P may  not be believed, and is unable to "hold that belief". Some people will be unable to have their knowledge expanded to include certain things (e.g. woman in the 60s may not even be able to express or understand the concept of sexual harrasment).

-> The concept that our knowledge is mainly testimony based is important here.
-> The abstract approach of knowledge and epistemology, does not take into account the real world capacities of our knowledge. For instance, we have to take into account that people are generally deeply irrational, we are deeply inclined to  not be rational, thus limiting their knowledge. 
\-> Naturalised epistemology: While it is still normative, unlike normal epistemology, takes into account how people function, and what people *can* know.
	\---> This is important in terms of `injustice' in that in a society where there are some people who are powerful enough to shape the way people think in the society (e.g. the vedic scriptures), there is issues with limiting of knowledge where some of the people are unable to attain certain knowledge due to the way they are brought up in society and their education. It is important to remember that what we can understand is limited by how we think about things.
	\-> How does race, gender and other social and cultural identities affect our knowledge of the real world.

-> We are going to be interested here in the various ways in which ignorance is constructed and different kinds of injustices are spread, and how these factors limit our knowledge and gaining of it. 
\-> the theme that underlies a lot of the articles is that people are not just victims, part of what you do as humans is fight your victimisation. 

p
-> The dominant culture is not always epistemologically advantaged, sometimes the non-dominant culture has the advantage of being an outside observer, provided that they are all aware of what is going on in the dominant culture as well. 

Mills: (Doing next time) one of the people who first talked about how ignorance is socially constructed and not just "Oh I didn't know that!" 
