\documentclass[twoside,titlepage,12pt,english]{article} %report for homework

\usepackage{babel}
\usepackage{times}
\usepackage{pifont}
\usepackage{moreverb} %for quotes etc.
\usepackage{alltt}  
\usepackage{setspace} %for double spacing.
\usepackage{indentfirst} %indent all first paragraphs too.
\usepackage{csquotes} %needed by below for some reason.
\usepackage[style=mla,natbib=true,citestyle=authoryear]{biblatex}
\bibliography{Essay}
\doublespacing %double spacing.
\usepackage{fullpage} %smaller margins, if you want.  

\newcommand{\wh}{\textit{world-hood} }
\newcommand{\pah}{\textit{present-at-hand} }
\newcommand{\rah}{\textit{ready-at-hand} }
%--------
\begin{document}

\title{Heidegger's Idealism and the Scientific Reality}
\author{Tejas (Tj) Hariharan \footnote{Licensed under a Creative 
Commons Attribution-Noncommercial-Share Alike 3.0 License. c.f. 
http://creativecommons.org/licenses/by-nc-sa/3.0/ for full license} \\ID: 20205237} 
\maketitle %title page.

%-------Start Main document.  
\section{Introduction}
In his essay on \textit{world-hood} Gordon (\citeyear{main}) expressed the concern that, 
Heidegger's ontology (specifically his view of \textit{world-hood}) is not 
compatible with his own (Heidegger's) claim that ``whereas worldhood is 
dependant upon practices and interpretative schemas, the scientific realist 
conceives of entities as `de-worlded'.''\autocite[][pg. 426]{main}. However, in 
my opinion Gordon does not give Heidegger's 
ontology enough credit in this respect.  

In this essay, I wish to show that, contrary to Gordon's presentation of 
Heidegger, Heidegger's notion of \textit{present-at-hand} vs 
\textit{ready-at-hand} \textbf{does} in fact present a way to remain essentially idealist whilst 
maintaining the notions of scientific realism (that science has in some ways a 
`semi-objective' view of reality). This presents us with a way to remain 
somewhat realist whilst still maintaining a semblance of idealism, to show 
this I shall compare this view with Kantian Idealism (and how the former deals 
with `scientific-realism'). To that end, I shall herein concern myself with 
only the parts of either philosophy that are relevant to my thesis, i.e.: that 
Heidegger's dichotomy (ready-at-hand vs present-at-hand) present a nice way of 
coming to terms with Scientific realism. 

This paper has 3 parts to it (a thinly veiled attempt at copying 
the flow of the paper by Gordon (2007)). First, I shall briefly introduce certain aspects 
of Kantian Idealism, this is done mainly to be able to have something to build 
on later on. Then, I shall introduce Heidegger's notion of \textit{world-hood} 
and \textit{present-at-hand} and \textit{ready-at-hand} (anglicized versions 
of \textit{vorhandenheit} and \textit{zuhandenheit} respectively), this I 
shall do by building on prior notions established whilst introducing 
Kantian Idealism - to stress the differences as well as the similarities in the 
two views. The two ideas introduced by Heidegger (mentioned above), offer an 
important difference between Kant and Heidegger (along with the consequences 
of Heidegger's definition of \textit{Dasein} as `being in the world'), and within 
this difference lies an interesting way for Heidegger to remain at least 
partially realist, the details of which will be shown in the final subsection of this paper.  
 
\section[Kant]{Kant's Idealism}
\subsection[Introduction]{A Brief Introduction to Kantian Idealism}
In order to discuss Heidegger's idealism and its responses to scientific 
realism, I feel it is important to first introduce Kantian Idealism, insofar 
as it is essentially the grandfather of Heidegger's theories. As mentioned 
before, I shall hereto concern myself with only those parts of Kant's theories 
that are relevant to my eventual thesis or its understanding. 

Kant's philosophy, or at least parts of it, can be seen as somewhat of a 
direct response to Hume's skepticism. Without going into the history of it 
all (which is beyond the scope of this paper), Kant's idea was to propose that 
certain aspects of our experience (such as cause and effect or mathematical 
truths) are synthetic apriori. What this means is that, while there is nothing 
in a cause that will grant the effects by sensory experience (cause and 
effect are merely principles that we observe as true) nor is there nothing 
within the notions of cause or effect that grant this special connection, we 
can know of this principle of cause and effect prior to any experience by 
synthetic reasoning (that is; such a truth as not self-evident). He further 
goes on to say that, there are certain principles then, that are thus formed 
apriori and pertain to a sort of ``pre-theoretical substrate of 
experience'' \autocite[][]{main}. He agreed with Hume, in that, there is nothing we 
can know beyond the limits of our own experiences, and consequently our own 
mental representations, the apriori principles then, offer a sort of framework 
around which the aforementioned representations are built.  

Kant introduces two terms, \textit{phenomena}, which is the world as we see it 
and as such is limited by our reason and the aforementioned `framework', and 
the \textit{noumena}, which is the world as it is, reality outside and 
independent of any mind, if there ever is such a thing. His final conclusion 
is that we can know nothing beyond the world of \textit{phenomena}. 
  
\subsection[Kant and Science]{Kant and \textit{Scientific Realism}}
The above view, is obviously idealistic, at least insofar as it is not a 
realist picture. However, Kant's ultimate aim was to put philosophy on the 
path of science, as it were \autocite[Preface$<$B$>$, Bxiv--xvi in ][pp. 
109--110]{cpr}, and not to merely render any sort of scientific inquiry 
useless. This is the danger of idealism, in that, if we were to continue with 
the conclusion made by Kant, then science has no access to the 
\textit{noumena}, in which case what use is there in doing such a science at 
all, as it is merely reduced to subjective ideas and false representations. 

Kant deals with this (or at least this is one way a Kantian might deal with 
this issue) by mentioning that there is indeed no reason to even think outside 
of the world of \textit{phenomena} and that all of our experiences are not 
only a dependant upon the \textit{framework} mentioned above (often known as 
the `Kantian categories'), but \textbf{necessarily} dependent upon it. What this means 
is that, every experience we have must conform with Kantian categories, and 
that these principles are inescapable. We thus arrive at a point where there 
is no reason in denying the possibility of such things as Logic and causality 
as they are what ``reason itself puts into nature'' \autocite[Preface$<$B$>$, 
Bxiv in ][pg. 109]{cpr}. 

Thus there is no need to worry about science being subjective, as this worry 
stems from the possibility of a removal of said subjectivity. But there is no 
such possibility. Science is objective, insofar as causality and logic are 
things that every thinking being must conform to his experience, and in an 
equal manner, for such is the universal nature of reason. This is a case of 
being a bit tricky of course, objectivity does in fact imply more than merely 
an agreement between everybody, but if the agreement is inescapable and 
instinctive as well as apriori then a functional notion of objectivity is at least 
reached. Science, then, rather than concern itself directly with nature, 
concerns itself with the facets of nature that are put there by reason, which 
means that science still is, the study of nature.         
 
\section[Heidegger]{Heideggerian Ontology: In Which I Show Its Differences from 
Traditional Idealism}
As a philosopher who is influenced by Kant, in his idealism, Heidegger can be 
well understood in Kantian terms. However there are many things that Heidegger 
introduces that would be foreign to Kant. Key to Heidegger's philosophy is his 
idea of `being in the world' or \textit{Dasein}. There are three other concepts 
in Heideggerian philosophy that I shall talk about here, and they are 
\textit{present-at-hand}, \textit{ready-at-hand}, and the general concept of 
\textit{world} and \wh that Heidegger has borrowed from his predecessors. 
  
\subsection[Introduction]{A Brief Introduction to Heideggerian Philosophy and its Differences 
from Kant}
Nothing we understand, we do so in Vacuum. A knife is a knife in 
virtue of its ability to cut other things, and also in virtue of its special 
nature to be useful in this regard to humans (and specifically being 
constructed to be so). This sort of `mesh' (if we may imagine it as so) is not 
only consisting of different objects we encountered and qualities, but also 
our experiences, and notions. For instance, someone who has knowledge of 
physics is much more likely to enjoy the news of a new particle discovery that 
someone who has not. Everything we experience then, is experienced through a 
sort of `mesh' or `filter', this `filter' Heidegger calls the \textit{world} (or at 
least this is one of the ways in which he uses the word). 

We can see immediately the connection to Kantian Idealism, the \textit{world} 
here, is little more than what I earlier presented to be \textit{kantian 
categories}. However there is an important difference here, in that reality 
seen through the \textit{world} is a little more subjective than the \textit{phenomena}. The most important difference in Heidegger's philosophy 
however (as I mentioned 
before), is likely Heidegger's notion of \textit{Dasein}, which is unique so 
far, to him. Dasein is basically a human, and this \textit{Dasein} is defined 
to be equivalent to \textit{being-in-the-world}. What this means is that we as humans cannot 
escape the \textit{world} we are \textit{in-the-world} and are part of it. To 
Heidegger, Dasein arises with the world, and is immediately a part of it. The 
\wh is a part of Dasein's (or a humans) structure insofar as his/her 
consciousness is concerned. The latter concept of course, is merely an 
extension of the idea of kantian categories as inescapable part of our 
interaction with the world. What is unique here is that not only is this world 
dependent upon Dasein for its being, but Dasein too is in some ways dependent 
upon the world for its being. For without the world, there is no 
\textit{being-in-the-world} and thus no \textit{Dasein}. 

\subsection[Heidegger and Kant]{The Dichotomy that Separates: Heidegger 
vs. Kant}

Heidegger, introduces two more concepts (among others) 
that I shall concern myself with, \pah and \rah. The \pah is essentially an 
object removed from its relation to other objects and essentially removed from 
the framework that we perceive the world with. For instance, normally a knife 
(or what we call a knife) is seen in relation to what it means to cut, what it 
means to be able to be used to cut by a human and so on. The \pah way of 
looking at the knife would regard it as merely a knife and nothing more. The 
\rah, is of course its opposite, it is relates to objects as it is seen 
through our \wh.   

This is quite different from anything Kant presented. Unlike the distinction 
between \textit{noumena} and \textit{phenomena} the above distinction lies in 
the \textit{phenomena}, and as such has to do with how we interact with the 
representations and ideas that are already present therein. The \pah is not 
the \textit{noumena}, but surprisingly close, in that it offers us a way of 
looking at the reality without some of the tint associated with the idea of 
\wh. However, \pah is still tied to our \textit{phenomenal} world, in that it 
is not, in fact, the world as it is, but merely our own representation of the 
world. \textit{Noumena} involves looking at the world without the notions of 
conditions or causality \autocite[Preface$<$B$>$ in ][pg. 110]{cpr}cpr, 
however \pah is merely looking at each object without any pre-conceived 
notions of what its relations is to another object or objects.    

\subsection[Heidegger and Science]{The Consequences of Heideggerian `realism' for Science.}

Throughout much of his life, as mentioned by Gordon (2007), Heidegger insisted 
on being a realist, at least insofar as he insisted that science has a sort of 
`privileged access' to a reality that is beyond the \textit{worldhood}. His 
philosophy, as Prima Facie idealism (at least), seems incapable of granting 
science this position of privilege. However the dichotomy he offers between \pah and \rah offers a way to hold this view. 

\textit{Ready-at-hand} is \textbf{not} \textit{present-at-hand}, this much is 
clear from the very fact that such a distinction is made, and from the 
definitions given of it, above. The former can be thought of as the way we see 
the world everyday, tainted through our experience and emotions. A big part of 
the \wh for Heidegger is actually the fact that we see things related to 
other things, and never by itself, which is a \rah way of looking at things. 
However, we may look at things in a \pah way, we are completely capable (as I 
am sure you can see) to see a knife as merely a construction of metal and wood 
and nothing more. It is no longer a `knife' as it were, but merely wood and 
metal, this is the \pah way of looking at things. 

This then offers us a nice way of objectifying science, or at least granting 
it some amount of objectivity. While, the notion of a worlded reality carries 
with it heavy amounts of subjectivity, due to the idea that part of it has to 
do with our personal experiences, a \pah way of looking at things offers a way 
of seeing objects without this tint of personalisation. 

\section{Conclusion}
Heidegger, thus does away with the idea that science cannot be afforded a 
non-subjective view of reality. While this is not completely realism, it is at 
least partly so. 

It must be mentioned here that Kant too, found a way to maintain the objective 
pursuit of scientific inquiry, in maintaining that his \textit{categories} are 
so inescapable as to be functionally equivalent to objective. We can see here 
how Heidegger's way of escaping this differs in a subtle way, in that it 
presents us with an alternative level of \textit{phenomena} or the world 
within the \textit{kantian categories} that escapes the basics of the 
subjectivity that mars the conception of \textit{world-hood}.
 
%-------start Bib-----
\nocite{*}
\printbibliography
%-------end Bib-------
\end{document}
