\documentclass[letterpaper,12pt,english]{article} %report for homework
\label{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{times} %font
\usepackage{ulem} %ul is now the new emph.
\usepackage{hyperref} %links
\usepackage{pifont} %extra font capabilities
\usepackage{moreverb} %for quotes etc.
\usepackage{alltt}  %typesetting stuff
\usepackage{setspace} %for double spacing.
\usepackage{natbib} %bibliography
\doublespacing %double spacing.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\title{Redefining Knowledge, Revaluing Ignorance}
\author{Tejas (Tj) Hariharan (20205237)} 
\maketitle %title page.
%\begin{abstract}
%  We have seen in \citeauthor{townley}, the importance of attending to a 
%positive role for ignorance. In doing so, we have also seen the harm that 
%Epistemophilia can cause. However, given how ingrained the idea of 
%Epistemophilia is within our system of knowledge and Epistemology, we must 
%seek to re-evaluate the objects of knowledge and epistemology are we to truly 
%be free from the grasp of Epistemophilia. 
%\end{abstract}
%-------start main Essay-------
\section{Introduction}
In Townley's (2006) article, she emphasises the importance of ignorance, to be 
seen not merely as an instrument for epistemophilia, but an end to itself. 
Along with dealing with instrumental ignorance (ignorance used instrumentally 
as a means to another end), she also talks about the importance of 
\textit{non-instrumental ignorance} ``as a part of inherently valuable 
epistemic relationships, for example, those involving trust and empathy.'' 
\citep[pg. 38]{townley}. In my opinion Townley brings up a very good point, 
that is, the need to recognise the importance of ignorance above and beyond 
being merely a tool to achieve more knowledge.  

However, in doing so, Townley herself, it seems, falls prey to an implicit 
bias of considering knowledge as important. We can see in her treatment of 
Empathy and the importance it places upon ignorance, for instance, that 
ultimately ignorance is used as a way of ensuring future success in epistemic 
endeavours (which, as I shall show later, remain at its core veritistic). In 
attempting to avoid making ignorance instrumental to the ``search for truth'', 
she places the importance of ignorance not by itself, but due in part to other 
concerns, such as social, political and so on (which in turn are usually 
concerns due to more epistemophilic reasons). 

Additionally her treatment of trust, and `other epistemic agents' leaves much 
to be desired, as there is no real sense in which what she portrays here can 
be really called ignorance, much less `ignorance for its own sake'. 
Furthermore, while we all recognise the importance of other agents, something 
she plays on in her detailing of the importance of trust, there is still no 
concrete reason as to why other agents are important to my epistemic goals, 
whatever they may be (another question left unanswered, given that my 
epistemic goals are no longer supposed to be on of pure \textit{veritism}) or 
what it means to be an `agent' as opposed to a mere object of knowledge.

At the core of these, and some other issues that come up in the paper (the 
treatment of Non-Human Intelligences as agents and so on, as I shall show 
later), lies the fact that we live in a society that still goes by the maxim 
``Knowledge is Justified True Belief'', in addition to giving knowledge an 
external locus, the above individualises knowledge, as belief is often first 
and foremost a personal issue. In this paper, I shall attempt to construct a 
picture of epistemology, that while simple enough to be familiar, addresses 
the issues brought up in Townley's article. My hope is, to show that ignorance 
is a necessary part of the system of knowledge, rather than something to 
tolerate for other reasons (be they epistemophilic or social). 

\section{Townley, and the Importance of Ignorance}
In the article ``Toward a Revaluation of Ignorance'', \citeauthor{townley} (2006) 
attempts, as I have said before, to show the importance of Ignorance. She 
deals with, here, two contexts under which ignorance is useful; \textit{Trust} 
and \textit{Empathy}. I shall not concern myself here with the reasons she 
gives for the importance of ignorance, as I feel Townley has done a well 
enough job of showing us why it is sometimes necessary to acknowledge our 
ignorances (This is very evident in her evaluation of the Bell/Huggins case), 
nor shall I concern myself with \textit{Instrumental Ignorance}, at least for 
now, as I feel the problems lie elsewhere. 

Her main concern in this article, is to fight the notion of 
\textit{epistemophilia} and a purely veritistic search for knowledge. By 
examining the importance of ignorance, she hopes (it seems) it becomes clear 
that a purely epistemophilic goal of ``as much knowledge as possible'' is not 
always what one should aim for. She shows this by considering Trust and 
Empathy, two very important factors in dealing with other epistemic agents.  

\subsection{Townley's Notions of Ignorance: Trust}
``In negotiating and navigating epistemic domains, we don't and shouldn't 
treat our fellows purely instrumentally, as tools to maximise our `veritistic 
outcomes' or collection of truths'' \citep[pg. 39]{townley}. This in essence 
is why trust is important. In treating other agents as objects, says Townley 
(2006, pg. 40), we limit their epistemic agency by not acknowledging them and 
their skills and contributions to our shared epistemic goals and ideas. The 
importance of ignorance comes in here, because in order to fully respect the 
other member of our epistemic community, we must be able to defer to the other 
person's authority and agency, and be comfortable in the ignorance that 
ensues. The importance of ``trust and its component ignorance'' \citep[pg. 
41]{townley}, comes then from the need to maintain successful relationships 
with other agents, and not from the nature of epistemology or epistemic 
inquiry itself. However, it must be said that the need to maintain 
relationships is itself important due to the structure of epistemic inquiry, 
as not an individualised subject, but one that is done in a communal setting. 
``[The other agents'] collaboration enables me to construct, adjust and 
fine-tune my understandings.'' \citep[pg. 40]{townley}. This brings the 
additional question (as mentioned above) of `who to consider as a full agent', 
since we are giving so much importance to full agency, it is of course 
necessary to identify to whom we bestow this honour. We cannot possibly call 
every source of information a full agent, and attempt to Trust them, that is 
irresponsible at best. The importance of clarifying ideas is important in any 
field of inquiry, when do we double-check, and when should we trust in the 
source? This is a question, perhaps of less importance than others raised 
here, that is not really addressed with Townley's notions of ignorance and the 
way she portrays it.   

\subsection{Townley's Notions of Ignorance: Empathy}
``In empathy, I have to see the other as a knower with her own perspective, 
one I have to strive (and might fail) to appreciate.'' \citep[pg. 
44]{townley}. Without going much into the importance of empathy, and its necessity, 
something that Townley (2006) covers in ample detail, I shall attempt here to 
briefly describe her treatment of Empathy and its ``component ignorance''. 
Empathy, although not defined as well as one would like in her article, is 
used to indicate the idea of recognising the other persons role in the 
subject (which is related to trust) as well as the other person's perspective 
and our own, and whatever biases and limitations it brings. For instance if a 
Man ever attempts to speak of ``female rape'', he must recognise that not 
being a woman brings with it the inherent limitation that he would not be able 
to speak of the topic in as much depth or with as much certainty as he might 
like. Being in an advantaged of privileged group brings with it additional 
responsibilities of course, especially when dealing with knowledge that is 
related to the other groups (for instance a white person studying aboriginal 
customs). This is well evidenced in Townley's treatment of the Bell/Huggins 
case (which I shall not go into here). While her treatment of Empathy does not 
lead to any additional problems (over and above the ones already present), it 
does give us an idea of the root of the importance of Ignorance, i.e.: the 
interrelatedness of knowledge and epistemic inquiry, both with other ideas as 
well as with people and instruments as sources of those ideas.   
 
\section{Re-Evaluating Our Epistemic System}
In recognising that the importance of ignorance lies in the interrelatedness of 
knowledge, I seek now to redefine the epistemic sphere, in such a way as to 
incorporate within the structure this nature of knowledge. Knowledge is 
traditionally defined as ``Justified True Belief'', a maxim that any 
epistemologist is all too familiar with. Any epistemic inquiry, even one that 
recognises the importance of other agents is ultimately doomed to follow this 
maxim and its pitfalls. Ignorance is always seen as anti-thetical to this 
search for knowledge, regardless of how important it may seem to be due to 
other concerns, ultimately, Ignorance is merely `tolerated', a `necessary 
evil', as it were. 

I hope here to provide an alternate picture of epistemology and its goals and 
objects, one that is in no small part influenced by my own understandings of 
the phenomenological treatment of epistemology. 
\subsection{The Objects of Knowledge Redefined}
Knowledge is ultimately, true or not, justified or not, consisting of ideas, 
like points in an ever-growing horizon of intent. We construct knowledge or 
talk about ideas by connecting many of these points together. Arguments, 
especially good ones, are essentially the art of connecting as many of these 
points together as needed, to form a structure upon which a new point can 
rest. This of course, doesn't mean every point has to be connected to others, 
or rest upon others, but this is often the case. We cannot see the world 
devoid of the horizon of intent, we see it merely through it. When I talk 
about a knife, I talk about it in terms of my ideas of `cutting', what it 
means to cut, what it means to be used in such a manner, what it means to be 
useful and be constructed for such a purpose and so on. Recognising this 
interconnectedness or not only ideas that already exist, but of new ideas, or 
knowledge we may acquire is important, because it is an essential part of how 
we think. The idea here is to think about knowledge as a set of intricately 
connected points, like `people' on a social network.  

Even people, are points, although this may seem a little dismal at first. When 
I talk about a person, I am talking about a set of related ideas, race, 
gender, relationships to others, expertise and so on. Some of these afford 
them connection to other ideas, for instance the fact that someone is a woman 
means that they are connected to other ideas I have such as pregnancy, 
menstruation and so on. If I then want to talk about pregnancy, I then see 
that there are already other `points' connected to this `idea' or point, one 
of them is (let's say) my mother. Thus, it is then plain to see why I would go 
ask my mother about pregnancy, I look up pregnancy in my little `network of 
ideas', and I see the point that says `mother' connected to it. An important 
point here to realise, is all through this, everything is very personalised. Your `network of ideas' or `horizon of intent' or whatever else you may call it, is your own, and no one else's. This has the additional outcome of not depending upon 
other minds, or the assumption that other people exist, or any ideas about 
intelligence and so on. However, connections are drawn between points on 
certain recognitions. For instance, realising that the person sitting next to 
me is knowledgeable in aboriginal rituals, means that I now connect the point 
that represent her, to the point that represents aboriginal rituals. This 
becomes important when we talk about when or why to trust another person.   

\subsection{Ignorance Redefined}
Ignorance in this new way of thinking about knowledge, becomes a part of the 
system. In any rational system, every point is supported by other points, 
which in turn are supported by more basic points and so on. My belief that I 
am a male, is supported by my doctor, and the ideas I have of what it means to 
be a doctor as well as of the doctor himself, in addition to that, there is 
the fact that people refer to me as a male, and my understandings and ideas 
(again seen here as points in the horizon) of what it means to be male, in 
terms of physical attributes and so on. I didn't need to justify my trust in 
the fact that my doctor told me I was male (let's assume for now that I am not 
experienced or old enough to understand physical differences and my social 
interactions as a male and so on), because the idea I have of a doctor offers 
enough support via other points it is connected to. I know he has a degree (in 
turn what it means to have a degree is related to other ideas which support my 
claim of the doctor offering enough support), I know he is reliable doctor due 
to experience both first and second-hand and so on. Attempting to look for 
more evidence then, is not irresponsible because it would show distrust in an 
authority on the subject (which it does), but merely because there is no need 
to offer more support to the point of ``I am a male''. 

One must also recognise that, the more that rests on one point the more 
support it needs, this is just like building any other structure. Certain 
ideas would deem more support, merely because they are more important, or more 
rests on their `correctness' than others. For instance when I collect money 
from somebody, it is very important that  I ensure that I have exactly what I 
think I do, in this case it may not be so rude or irresponsible if I double 
check, by counting them myself. But my ideas of my own abilities to count, may 
deem it unworthy of such an important task, because perhaps I know that I tend 
to miscount things, in this case I would ask somebody else to count, or 
perhaps count again, as I have the idea that it is harder to make the exact 
same mistake many times in a row. 
 
\subsection{Townley and Ignorance}
We can already see the importance of empathy here. Certain people (or rather 
ideas I have about them) end up being intimately connected to so many points 
and ideas, that it is important to recognise their point independent of other 
things, such as my own view. Built-in to this view, is a notion of 
subjectivity, that ultimately all you know is what you know from your own 
point of view, and other people afford you a view from other places. Ignorance 
is not seen as a means to something else, such as maintaining relationships or 
gaining more knowledge, but a part of the system. There is no need to do more 
work, unless the situation deems it, so why do the extra work? This is key to 
idea of ignorance, in my opinion. The idea I have of my professor connects her 
to so many points about feminism that there is no real reason to seek support 
from elsewhere, just connecting some point to the `professor' point pulls in 
all the support structure that is part of related to the `professor' (such as 
the fact that she is well known in her field, and she has not steered me wrong 
so far and so on). This idea of ignorance, as built into the system, addresses 
other types of ignorances as well. There are ideas that simply to not exist in 
my `horizon of intent', but by referring to people or objects I pull in 
everything that they are familiar with as point that they are connected to. 
So, for instance, it's Okay for me to be unfamiliar about the details of 
anatomy, because I have the idea of a doctor, who is intimately familiar with 
such details, and I can refer to this idea whenever I need support or inquiry 
about anything relating to anatomy. Of course, we must know who know about 
what, and which object can give us information about what ideas and points, 
and to what extent. Sometimes the supporting structure brought in by an object 
is simply not enough, for instance when I am counting money with my own hands 
and eyes, there is no idea I have that offers enough support on the basis of 
my own abilities to make me comfortable in saying that I have enough reason to 
think that I know how much money I have.   
\section{Conclusion}
There are even more additional advantages to this system. One major one is 
that the ideas of agency are no longer resting upon flimsy notions of 
`intelligence' which are ill defined enough even by experts in the field. The 
amount of trust and empathy and `membership' you have in my community, is 
dependent upon how much you bring in with you. A human being, due to the fact 
that my idea of a human necessitates that I conform to him/her a sense of 
autonomy and intelligence (whatever that may mean), brings in more support for 
his/her points than a calculator or a thermometer, based again, merely on the 
ideas I have of each of these objects. Knowledge is not ``Justified true 
belief'' but ``a web of interconnected points and ideas in an ever growing 
sphere of intent'', every idea supported by others, the only way to keep a 
point on this horizon or sphere is to tie it to other points, or else it will 
fall off. By painting this picture of knowledge, I hope to have shown the 
interconnected nature of knowledge, and that knowledge does not have an 
external locus, but an internal one. Knowledge, and the definition of what is 
considered to be reasonable and so on, comes from within the system and the 
person defining it, and not from outside. Ignorance has become part of the 
system, and has thus acquired a new value and meaning, as more than just a 
tool, something to be `tolerated' and so on. I hope by doing this, I have 
addressed some of the issues I had with Townley's (2006) article, as well has 
taken a step forward in redefining knowledge to better suit our current 
understanding of how epistemic responsibility and ideas should work.  
%-------start Bib-----
\nocite{*}
\bibliography{Essay} %needs Essay.bib
\bibliographystyle{apa} 
%-------end Bib-------
\end{document}
