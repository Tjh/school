\documentclass[a4paper,12pt,english]{report} %report for homework

\usepackage{babel}
\usepackage{times}
\usepackage{pifont}
\usepackage{moreverb} %for quotes etc.
\usepackage{alltt}  
\usepackage{setspace} %for double spacing.
\usepackage{draftcopy}%for drafting
\usepackage{natbib}
\doublespacing %double spacing.

\begin{document}

\title{Assignment 2: Consciousness and Computation-Representation Model}
\author{Tejas (Tj) Hariharan \footnote{Licensed under a Creative 
Commons Attribution-Noncommercial-Share Alike 3.0 License. c.f. 
http://creativecommons.org/licenses/by-nc-sa/3.0/ for full license}} 
\maketitle %title page.

%-----
\chapter{Introduction}
\section{The problem of consciousness}
Consciousness; we all experience it, yet very few know little of anything 
about it. Especially now, with the advent of more and more powerful machines, 
or other human made constructs, we seem to be at a crossroads as to what 
consciousness really is. One of the major concerns of Cognitive Scientists as 
well as Philosophers, in this subject, is the one question ``What is 
consciousness?'', or to be more precise  ``Can we make a machine that 
thinks''. In this essay I shall try to present a view that possibly answers 
that question.   

One of the main reason, I feel, for confusion in this field is due to the 
words \emph{Consciousness} as well as \emph{Machine}. They both can have 
various meanings, and the meaning of both can change over time. For example, 
if two engineers got together and had a baby, this baby would be defined as a 
Human.  Yet, if the same two people through different process created a robot 
(chunk of metal that moves around and reacts to stimuli) this would be defined 
as a robot, not a human. In this case the distinction is clear cut, one is a 
chunk of metal, the other is a human, the latter being more complex and 
containing biological systems. However what of the concept of ``Androids'' or ``Cyborgs''.
For example, what of the so called ``robots'' that are presented in the 
animé/manga Ghost in the shell, and other sci-fi movies/books.  As an example, 
in the world of \textit{Ghost in the Shell}, human bodies can be fully or 
partially replaced by machine, and machine can be replaced on a robot with 
functioning human or biological systems. However in this world there is a 
clear cut definition of ``consciousness'', it is called ghost.  A ``ghost'' 
simply put is a soul or the mind, of a person.  It is what separates me from a 
robot who looks and behaves just like me.  And the main character 
Major Kusanagi, is seemingly just a machine with a ghost of a human. In fact 
her title comes from the life she supposedly had before coming into her 
current form as a cyborg. This presents various issues, that is essentially 
the concern of a lot of philosophers and scientists alike. A very famous 
dialogue in the manga, succinctly surmises the depth of the issue 
at hand:

\begin{quotation}
\textbf{Major Motoko Kusanagi:} That robot.   Did we seem similar to you?\\
\textbf{Batou:} Of course not.  \\
\textbf{Major Motoko Kusanagi:} No, I don't mean physically.  \\
\textbf{Batou:} Just what, then?\\
\textbf{Major Motoko Kusanagi:} Well, I guess cyborgs like myself have a tendency to be 
paranoid about our origins.   Sometimes I suspect I am not who I think I am, 
like maybe I died a long time ago and somebody took my brain and stuck it in 
this body.   Maybe there never was a real me in the first place, and I'm 
completely synthetic like that thing.  \\
\textbf{Batou:} You've got human brain cells in that titanium shell of yours.   You're 
treated like other humans, so stop with the angst.  \\
\textbf{Major Motoko Kusanagi:} But that's just it, that's the only thing that makes me 
feel human.   The way I'm treated.   I mean, who knows what's inside our 
heads? Have you ever seen your own brain?\\
\textbf{Batou:} It sounds to me like you're doubting your own ghost.  \\
\textbf{Major Motoko Kusanagi:} What if a cyber brain could possibly generate its own 
ghost, create a soul all by itself? And if it did, just what would be the 
importance of being human then?\\
\end{quotation}
In this case, the belief is that there is a ``ghost'' that separates humans 
from any kind of robots no matter how much biological entity it may contain. 
The ghost in this case would correspond to what most people think of as 
consciousness.  

But how true is this interpretation, is there really some distinct ``thing'' 
that separates us from a robot. As pointed out by the major, what if a 
mechanical system could create a ghost, would it then become human? And most 
worrying to the major, is the existence of her own ghost, as she pointed out, 
we know little about what really goes on in our brains in terms of 
consciousness, and similar for her own, what if she never had a ghost, she 
believes that she is a human whose body has been replaced by machine, but how 
far can such a belief take someone?

Of course on can simply deny the existence of a ``ghost'' taking a 
materialistic stand to all this, by saying that there is nothing in this world 
that is not rooted in matter. Thus a robot can be created that has a 
consciousness, and there would be nothing special about this robot, save its 
complexity perhaps. This is the view I shall present in this essay.  

 
\chapter{Consciousness and Computer Systems}
\section{Defining Consciousness}

The first thing that one has to do is to define consciousness. Most would 
agree to the loose definition that consciousness is the ``ability to be aware 
of and react to our environment''. In the article on Consciousness and 
Nueroscience \citep{Crick_Koch}, the authors Crick and Koch, postulate on the 
at least the most important type of consciousness to us (Visual consciousness).

\begin{quotation}
We have argued elsewhere (Crick and Koch, 1995a) that to be aware of an object 
or event, the brain has to construct a multilevel, explicit, symbolic 
interpretation of part of the visual scene.   By multilevel, we mean, in 
psychological terms, different levels such as those that correspond, for 
example, to lines or eyes or faces.   In neurological terms, we mean, loosely, 
the different levels in the visual hierarchy. \citep{Crick_Koch}
\end{quotation}

He then in the same article goes on to speak of an experiment that proves his 
hypothesis about the neural underpinnings of visual consciousness. He says in 
short, that visual consciousness is essentially a system of neurons firing in 
certain manner. Thus, consciousness is nothing but a complex system of 
neurons. Since there is no scientific evidence to think otherwise, and so far 
various experiments have agreed to the conclusion that consciousness is 
essentially a property of a complex system of neurons and nothing more
\citep[pp. 181 - 185]{mind}, we can safely say this is our best option so far.  

However as I said before the problem starts there. The real question is, is 
it then possible to create a machine that can imitate these neural networks. 
At first glance the answer would be a resounding yes, even though we may not 
have the technology to do just such a thing yet, history has proven to us that 
our technology could improve by immense amount in 100 years. \citep{mind}. But 
will it ever be enough? The problem start multiplying if one takes into 
account creatures such as Robocop (from movie of the same name), or Major 
Kusanagi from the \textit{Ghost in the Shell} series. How human are they 
really, do they really count as machines, or is there a point where the are 
basically humans with some parts replaced (since in the latter case we would 
probably have no issue regarding their consciousness).

\section{Of Men and Machines}
As in the example in the Introduction to this essay, we seem to have an issue 
of where to draw the line. The concern we have stems from the difference we 
perceive between us as humans and a robot we built, which may look like us (it 
is not unimaginable to think of robots that are built with outer layers of 
material that feel and look like human skin, or perhaps human skin itself). 
For example, we know that a baby that is created by two humans is human, due 
to the process involved, but what if the baby was a test tube baby? Perhaps a 
child of 2 men? The process then is complex, and seemingly at the very least 
not clear cut and natural, to say the least. On the other hand, let us imagine 
a machine created, that has biological parts grown using stem cells in a lab, 
and that it is a robot that utilizes advanced AI. We would like to say the 
latter is not a biological entity, whereas the former one is? 

On the one hand we would not be surprised, or even question the consciousness 
of the test tube baby, but we may question the consciousness of the 
Bio-Mechanical cyborg. However the distinction does not seem to be so clear 
cut, what makes something biological and ``alive'' as opposed to a non living 
machine that is ``programmed'' to react in a certain way. Is there anything 
that can separate the two, and thus essentially define what we may call 
``consciousness'' or at least the root cause of it?

\section{An Empty Shell}
The famous phrase ``ghost in the shell'' actually stems from a philosophers reaction to Descartes 
dualism. He says that, Descartes' dualism seems to suggest that we are simply 
a mind that are stuck in a material body, a ghost in a machine, that controls 
the machine. Thus, when the body gets hurt, it is not us who are really hurt 
but simply our vessels.  

Due to this and other issues modern man has moved away from a dualist view 
point and more toward a materialistic point of view. We say today, science 
proves that our conscious experience is nothing but a translation or 
representation of chemical and neurological processes. 

But if that were the case, then we encounter problems such as the one in the 
last section. Where then do we draw the line between a human and a robot. If 
all that is consciousness is physical process, then a sufficiently well 
designed robot could in essence be as a human, perhaps even surpass us.  

As shown above the ghost in the shell scenario can very well account for our 
difference with the machines, not only that such an understanding may even 
pave the way for a future where we can load this ghost into a man made shell, 
much like the futuristic society in \textit{Ghost in the shell}. We then have 
a clear cut distinction between something that is alive and something that is 
not. 

I shall adopt the materialistic way of thinking. But as I have proven above 
this means that we cannot in essence make a clear cut distinction between us 
and a cyborg. We could very well build a cyborg that contains a human brain, 
or parts of one, much like Major Kusanagi, or replace most of our body with 
machine like robocop, the distinction then is thin, and shows its true nature 
as merely this, a distinction created by us to facilitate our view of the 
world (much like what Kant says about time and space).
%----


\nocite{*}
%-------start Bib-----
\bibliography{Essay} %needs Essay.bib
\bibliographystyle{mla} 
%-------end Bib-------
\end{document}

