<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>File: Assignment #1 Solutions: page 1 (jpg)</title>
<link rel="STYLESHEET" type="text/css" HREF="/uwangel/Stylesheets/default.css">
<script type="text/javascript" language="JavaScript">
<!--//
var gblnRefreshIndex = false;
var gstrAPI = '/uwangel/api/default.asp';
var gstrAngelApp = 'CRSCNT';
var gstrWCI = 'PGDISPLAY';
var gstrWCE = 'RESPOND';
var gstrFinalWCI = 'PGDISPLAY_FILE';
var gstrFinalWCE = 'RESPOND';
var gstrScriptURL = 'https://uwangel.uwaterloo.ca/uwangel/section/content/default.asp?WCU=CRSCNT';
var gstrPostURL = 'https://uwangel.uwaterloo.ca/uwangel/section/content/default.asp?WCI=pgPost&WCU=CRSCNT&ENTRY_ID=BCB30894F81D4064ACCD2C4DFD8EA556';
var gstrBaseHref = 'https://uwangel.uwaterloo.ca/AngelUploadsuwangel/Content/UW-MCL-C-071217-144239/_assoc/BCB30894F81D4064ACCD2C4DFD8EA556/';
var gstrStylesheet = '/uwangel/Stylesheets/default.css';
var glngUserRights = 2;
var gstrUserID = 'tharihar';
var gstrSectionID = 'UW-MCL-C-071217-144239';
var gstrRootID = 'ROOT';
var gstrParentID = '6780420BFDF344D7806BB2E5F45782A5';
var gstrObjectID = '789BA82700C642108064F1B60A1CB34D';
var gstrObjectType = 'FILE';
var gstrBaseType = 'FILE';
var gstrEntryID = 'BCB30894F81D4064ACCD2C4DFD8EA556';
var gstrEntrySection = 'UW-MCL-C-071217-144239';
var gstrEntryTitle = 'Assignment #1 Solutions: page 1 (jpg)';
var gstrBoardID = '';
var gstrBoardTitle = '';
var gstrShortcutID = '';
var gstrShortcuts = '';
var gblnSubmit = true;
var gblnForceSubmitting = false;
//-->
</script>

<script type="text/javascript" language="JavaScript">
<!--//
function framesetLoaded() {

}
//-->
</script>
<script language=JavaScript src="/uwangel/Timeout/TimeoutJS.asp"></script>
<script language="Javascript">

function findAPIWin(w) {
	if (parent.AngelSCORMAPI) { return parent; }
	if (parent.parent.AngelSCORMAPI) { return parent.parent; }
	if (opener) { 
		if (opener.AngelSCORMAPI) { return opener; }
		if (opener.parent.AngelSCORMAPI) { return opener.parent; }
	}
	return null;
}
this.APIWin = findAPIWin(self);
if (this.APIWin != null) { 
	if (APIWin.API.entry_id != gstrEntryID || APIWin.API.item_id != 'DEFAULT') { 
		APIWin.InitAngelSCORM(gstrEntryID, 'DEFAULT');
	}
	this.API = APIWin.API;
	if (gstrObjectType != 'MESSAGE' && gstrObjectType != 'QUESTION' && gstrObjectType != 'FIELD') { 
		if (this.APIWin == parent || (opener && (this.APIWin == opener.parent))) { 
			this.APIWin.SectionAPI.last_id = gstrEntryID;
			this.APIWin.SectionAPI.last_title = gstrEntryTitle; 
		}
	}
}
</script>

<script language="Javascript">
<!--//
var bNoBreadcrumb = true;
function updateCurrentItem() {
   var w = self.parent;
   if (!w.angelMenuScript) { w = self.parent; }
   // Attempt to update breadcrumbs
   if (w.ANGEL_BREADCRUMBS && '' != gstrEntryID) { 
      var l = w.ANGEL_BREADCRUMBS.location; 
      if (gstrBoardID != '') {
         l.replace(l.protocol + '//' + l.hostname + l.pathname + '?entry=' + escape(gstrEntryID)); 
      } else {
         if (gstrWCI == '' || gstrWCI == 'PGDISPLAY' || gstrWCI == 'NEXT' || gstrWCI == 'PREVIOUS' || gstrWCI == 'UP' || gstrWCI == 'TOP') {
            l.replace(l.protocol + '//' + l.hostname + l.pathname + '?entry=' + escape(gstrEntryID)); 
         } else {
            l.replace(l.protocol + '//' + l.hostname + l.pathname + '?pglabel=' + escape(self.document.title) + '&pgurl=' + escape(self.location.href)); 
         }
      }
   } 
   // Attempt to update map selection
   if (w.angelMenuScript && '' != gstrEntryID) {  
      updateMap();
      if (gblnRefreshIndex && parent.ANGEL_COURSE_NEW) { 
         if (parent.ANGEL_COURSE_NEW.refreshMap) { 
            parent.ANGEL_COURSE_NEW.refreshMap();
         }
      }
    }
 }
 function updateMap(){
   var w = self.parent;
   if (!w.angelMenuScript) { w = self.parent; }
   var strSelected = '';
   var strNewSelect = '';
   if (w.angelMenuScript && '' != gstrEntryID) {  
      if (w.currentEntry) { strSelected = w.currentEntry(); }
      strNewSelect = gstrEntryID; 
      if (gstrBoardID != '' && gstrBaseType == 'MESSAGE') { strNewSelect = gstrBoardID; } 
      if (gstrBaseType == 'QUESTION' || gstrObjectType == 'QUESTION') { strNewSelect = gstrParentID; } 
      if (gstrBaseType == 'FIELD' || gstrObjectType == 'FIELD') { strNewSelect = gstrParentID; } 
      //if (gstrShortcutID != '') { strNewSelect = gstrShortcutID; }  
      if (gstrWCI == '') { strNewSelect = 'content'; } 
      if (strNewSelect == gstrRootID) { strNewSelect = 'content'; } 
      if ((gstrShortcutID != '') && (gstrEntrySection == gstrSectionID)) { w.gbShortcut = true; } else { w.gbShortcut = false; } 
      if (!w.setCurrentEntry(strNewSelect)) { 
         if ((!w.gbShortcut) && (gstrObjectType != 'MESSAGE') && (gstrObjectType != 'QUESTION') && (gstrObjectType != 'FIELD')) { 
            w.clearCurrentEntry(); 
         } 
      }
   }
}
setTimeout('if (self.frames.length > 0) updateCurrentItem()', 500);
//-->
</script>

</head>
<frameset framespacing="0" rows="55,*" onload="framesetLoaded()">
  <frame name="lsn_header" longdesc="Resource title frame with navigation menu" src="https://uwangel.uwaterloo.ca/uwangel/section/content/default.asp?WCI=pgBanner&WCU=CRSCNT&PREVIEW=0&ENTRY_ID=BCB30894F81D4064ACCD2C4DFD8EA556" frameborder="0" scrolling="no" target="_parent">
  <frame name="lsn_page" longdesc="Current resource" src="https://uwangel.uwaterloo.ca/uwangel/section/content/default.asp?WCI=pgFileLink&WCU=CRSCNT&ENTRY_ID=BCB30894F81D4064ACCD2C4DFD8EA556" frameborder="0">
  <noframes>
  <body class="bodyLessons"><div class="normalDiv"><div class="normalSpan">
<div><big><strong><span class="pageTitleSpan"><a class="largeIcon" href="https://uwangel.uwaterloo.ca/uwangel/section/content/default.asp?WCI=UP&WCU=CRSCNT&ENTRY_ID=BCB30894F81D4064ACCD2C4DFD8EA556"><img class="largeIcon" src="https://uwangel.uwaterloo.ca/uwangel/images/icons/iconFILE.gif" border="0" hspace="2" vspace="1"  style="vertical-align:text-bottom"  alt=" Up "  height="24"></a>Assignment #1 Solutions: page 1 (jpg)</span></strong></big></div><table summary="" width="100%" border="0" cellspacing="0" cellpadding="2" style="margin-top: 2px; margin-bottom: 8px;">
<tr><td class="toolbarDiv" nowrap><div id="toolbar" name="toolbar" title="toolbar"><div><strong><small><span class="toolbarSpan">&nbsp;</span></small></strong></div></div></td><td class="toolbarDiv" align="right" nowrap><div id="navigation" name="navigation" title="navigation"><div><strong><small><span class="toolbarSpan">&nbsp;<a href="/uwangel/section/Content/Notes/edit.asp?id=BCB30894F81D4064ACCD2C4DFD8EA556&title=Assignment+%231+Solutions%3A+page+1+%28jpg%29&src=UW%2DMCL%2DC%2D071217%2D144239"  target="winNotes" onclick="window.open('', 'winNotes', 'width=550,height=375,menubar=1,resizable=1,scrollbars=1')" class="toolbarLink" >My Notes</a>&nbsp;&nbsp;<a  class="toolbarSpan" >|</a>&nbsp;&nbsp;<a href="https://uwangel.uwaterloo.ca/uwangel/section/content/default.asp?WCI=PREVIOUS&WCU=CRSCNT&navbar=1&ENTRY_ID=BCB30894F81D4064ACCD2C4DFD8EA556"  class="toolbarLink">Previous</a>&nbsp;&nbsp;<a href="https://uwangel.uwaterloo.ca/uwangel/section/content/default.asp?WCI=NEXT&WCU=CRSCNT&navbar=1&ENTRY_ID=BCB30894F81D4064ACCD2C4DFD8EA556"  class="toolbarLink">Next</a>&nbsp;&nbsp;</span></small></strong></div></div></td></tr>
</table><!--*EXECUTE*--><p><strong><span class="headingSpan"><a href="/AngelUploadsuwangel/Content/UW-MCL-C-071217-144239/_assoc/BCB30894F81D4064ACCD2C4DFD8EA556/hw1page1.jpg" target="_blank" >hw1page1.jpg</a></span></strong></p>
  </div></div></body>
  </noframes>
</frameset>
</html>